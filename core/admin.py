from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Region)
admin.site.register(Comuna)
admin.site.register(TipoVivienda)
admin.site.register(Perfiles)
admin.site.register(Usuario)
admin.site.register(Estado)
admin.site.register(Perro)
admin.site.register(Raza)
admin.site.register(FichaAnimal)
admin.site.register(AdopcionPerro)
