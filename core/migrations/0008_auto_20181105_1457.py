# Generated by Django 2.1.2 on 2018-11-05 17:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20181105_1034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='adopcionperro',
            name='idFichaAnimal',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.FichaAnimal'),
        ),
        migrations.AlterField(
            model_name='perro',
            name='idRaza',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Raza'),
        ),
    ]
