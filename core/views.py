from django.shortcuts import render
from .models import *
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils import timezone
import time
import smtplib

# Create your views here.

def index(request):
    return render(request, 'core/index.html')


def logout(request):
    auth.logout(request)
    return render(request, 'core/index.html')


def login(request):
    if request.POST:
        usu = request.POST["txtUser"]
        pas = request.POST["txtPass"]
        user = auth.authenticate(username=usu, password=pas)
        if user is not None and user.is_active:
            auth.login(request, user)
            # che==request.user.is_staff

            return render(request, 'core/index.html', {'usuario': user.username})
        else:
            return render(request, 'core/error.html')

    return render(request, 'core/login.html')


def formulario2(request):
    vivienda=TipoVivienda.objects.all()
    regi=Region.objects.all()
    comu=Comuna.objects.all()
    perfil=Perfiles.objects.all()
    if request.POST:
        email=request.POST.get("txtemail","")
        pas=request.POST.get("txtPassword","")
        nom=request.POST.get("txtNombre","")
        run=request.POST.get("txtRun","")
        tel=request.POST.get("txtTelefono","")
        fech=request.POST.get("dtFechanaci","")
        reg=request.POST.get("cboRegion","")
        com=request.POST.get("cboComuna","")
        vivi=request.POST.get("cboVivienda","")
        perf=request.POST.get("cboPerfiles","")
        usuario=request.POST.get("txtUser","")
        obj_region=Region.objects.get(name=reg)
        obj_comuna=Comuna.objects.get(name=com)
        obj_tipovivienda=TipoVivienda.objects.get(name=vivi)
        obj_perfiles=Perfiles.objects.get(name=1)
        usu= Usuario(
            name=email,
            passw=pas,
            rut=run,
            nombre=nom,
            FechaNaci=fech,
            Telefono=tel,
            idRegion=obj_region,
            idComuna=obj_comuna,
            idTipoVivienda=obj_tipovivienda,
            idPerfiles=obj_perfiles
        )
        usu.save()
        user = User.objects.create_user(usuario, email, pas)
        user.last_name = nom
        print(perf)
        if perf=="2":
            user.is_staff=True
        user.save()
        return render(request,'core/formulario2.html',{'reg':regi,'com':comu,'vivi':vivienda,'perf':perfil,'grabo':True})
    else:
        return render(request,'core/formulario2.html',{'reg':regi,'com':comu,'vivi':vivienda,'perf':perfil,'grabo':False})



@login_required
def registroMascota(request):
    raza = Raza.objects.all()
    estado = Estado.objects.all()
    if request.POST:
        nombre = request.POST.get("txtNombrePerro", "")
        des = request.POST.get("txtDescripcion", "")
        est = request.POST.get("cboEstado", "")
        raz = request.POST.get("cboRaza", "")
        fot = request.FILES.get("fileFoto")
        obj_raza = Raza.objects.get(name=raz)
        obj_estado = Estado.objects.get(name=est)
        mascots = Perro.objects.all()
        ia = 1
        for i in mascots:
            ia = ia+1
        ids = ia
        perro = Perro(
            name=ids,
            nombrePerro=nombre,
            descPerro=des,
            idEstado=obj_estado,
            idRaza=obj_raza,
            imagenPerro=fot
        )
        perro.save()
        return render(request, 'core/registroMascota.html', {'raz': raza, 'est': estado, 'grabo': True})
    else:
        return render(request, 'core/registroMascota.html', {'raz': raza, 'est': estado, 'grabo': False})

@login_required
def listarMascota(request):
    m = Perro.objects.all()
    contexto = {'m': m}
    return render(request, 'core/listarMascota.html', contexto)


def error_acceso(request):
    return render(request, 'core/error_acceso.html')


def modificarMascota(request):
    perro = Perro.objects.all()
    raza = Raza.objects.all()
    estado = Estado.objects.all()
    if request.POST:
        accion = request.POST.get("btnAccion")
        if accion == "Buscar":
            idp = request.POST.get("cboSeleccionar")
            perr = Perro.objects.get(name=idp)
            return render(request, 'core/modificarMascota.html', {'perro': perro, 'peru': perr, 'raz': raza, 'est': estado})
        else:
            try:
                idp = request.POST.get("cboSeleccionar")
                print(idp)
                nom = request.POST.get("txtNombrePerro")
                des = request.POST.get("txtDescripcion")
                ra = request.POST.get("cboRaza")
                obj_raza = Raza.objects.get(name=ra)
                es = request.POST.get("cboEstado")
                obj_estado = Estado.objects.get(name=es)
            except Exception as i:
                return render(request, 'core/modificarMascota.html', {'perro': perro, 'error': True})
            if nom != "" and des != "" and ra != "" and es != "":
                try:
                    perr = Perro.objects.get(name=idp)
                    perr.nombrePerro = nom
                    perr.descPerro = des
                    perr.idEstado = obj_estado
                    perr.idRaza = obj_raza
                    perr.save()
                    return render(request, 'core/modificarMascota.html', {'perro': perro, 'peru': perr, 'raz': raza, 'est': estado, 'conf': True})
                except Exception as i:
                    return render(request, 'core/modificarMascota.html', {'perro': perro, 'error': True})
                return render(request, 'core/modificarMascota.html', {'perro': perro, 'peru': perr, 'raz': raza, 'est': estado, 'conf': True})
            else:
                return render(request, 'core/modificarMascota.html', {'perro': perro, 'error': True})
    else:
        return render(request, 'core/modificarMascota.html', {'perro': perro})


def eliminarMascota(request):
    perro = Perro.objects.all()
    resp = False
    if request.POST:
        idp = request.POST.get("cboSeleccionar")
        perr = Perro.objects.get(name=idp)
        perr.delete()
        resp = True
    return render(request, 'core/eliminarMascota.html', {'perros': perro,'respuesta': resp})

def adoptar(request):
    perro=Perro.objects.all()
    f=FichaAnimal.objects.all()
    ad=AdopcionPerro.objects.all()
    if request.POST:
        idf=1
        idPerro=request.POST.get("oculto")
        obj_perro=Perro.objects.get(name=idPerro)
        est=Estado.objects.get(name=1)
        for i in f:
            idf=idf+1
        idf=idf+1
        obj_perro.idEstado=est
        
        ficha= FichaAnimal(
            name=idf,
            fechaFicha=time.strftime("%x"),
            idPerro=obj_perro
        )
        ficha.save()
        idad=1
        for d in ad:
            idad=idad+1
        idad=idad+1
        username=request.user.last_name
        obj_user=Usuario.objects.get(nombre=username)
        adopcion=AdopcionPerro(
            name=idad,
            FechaAdopcion=time.strftime("%x"),
            idFichaAnimal=ficha,
            Email=obj_user
        )
        adopcion.save()
        obj_perro.save()
        return render(request,'core/adoptar.html',{'Perro':perro,'conf':True})
    else:
        return render(request,'core/adoptar.html',{'Perro':perro})

def adopciones(request):
    adop=AdopcionPerro.objects.all()
    return render(request,'core/adopciones.html',{'adop':adop})



def recuperar(request):

    if request.POST:
        usuarion=request.POST.get("txtUsuario")
        print(usuarion)
        try:
            user=Usuario.objects.get(name=usuarion)
        except Exception as ex:
            return render(request,'core/recuperar.html',{'noexiste':True,'wtf':ex})
        us=user.nombre
        passa=user.passw
        email=user.name
        
        
        ms='Hola '+us+' gracias pos usar nuestro sistema de recuperacion de usuario su clave es '+passa
        
        fromaddr = 'misperris5@gmail.com'
        toaddrs  = email
        msg = ms
        username = 'misperris5@gmail.com'
        password = 'misperris1998'
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(username,password)
        server.sendmail(fromaddr, toaddrs,msg)
        server.quit()

        return render(request,'core/recuperar.html',{'correo':True})
    else:
        return render(request,'core/recuperar.html')

