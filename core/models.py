from django.db import models

# Create your models here.

class Region(models.Model):
    name=models.IntegerField(max_length=4, primary_key=True,verbose_name="idRegion")
    descripcion=models.CharField(max_length=50)
    def __str__(self):
        return self.descripcion

class Comuna(models.Model):
    name=models.IntegerField(max_length=4, primary_key=True,verbose_name="idComuna")
    descripcion=models.CharField(max_length=50)
    idRegion=models.ForeignKey(Region,on_delete=models.CASCADE)
    def __str__(self):
        return self.descripcion

class TipoVivienda(models.Model):
    name=models.IntegerField(max_length=4, primary_key=True,verbose_name="idTipoVivienda")
    descripcion=models.CharField(max_length=30)
    def __str__(self):
        return self.descripcion

class Perfiles(models.Model):
    name=models.IntegerField(max_length=4, primary_key=True,verbose_name="idPerfiles")
    descripcion=models.CharField(max_length=30)
    def __str__(self):
        return self.descripcion

class Usuario(models.Model):
    name=models.CharField(max_length=255, primary_key=True,verbose_name="Email")
    passw=models.CharField(max_length=15)
    rut=models.CharField(max_length=12)
    nombre=models.CharField(max_length=45)
    FechaNaci=models.CharField(max_length=12)
    Telefono=models.IntegerField(max_length=11)
    idRegion=models.ForeignKey(Region,on_delete=models.CASCADE)
    idComuna=models.ForeignKey(Comuna,on_delete=models.CASCADE)
    idPerfiles=models.ForeignKey(Perfiles,on_delete=models.CASCADE)
    idTipoVivienda=models.ForeignKey(TipoVivienda,on_delete=models.CASCADE)
    def __str__(self):
        return self.nombre

class Estado(models.Model):
    name=models.IntegerField(max_length=4, primary_key=True,verbose_name="idEstado")
    descripcion=models.CharField(max_length=50)
    def __str__(self):
        return self.descripcion

class Raza(models.Model):
    name=models.IntegerField(primary_key=True,verbose_name="idRaza") #name: solo primaria(siempre)
    descripcion=models.CharField(verbose_name="descripcion",max_length=30) #revisar en caso de terremoto en el codigo
    def __str__(self):
        return self.descripcion

class Perro(models.Model):
    name=models.IntegerField(primary_key=True,verbose_name="idPerro")
    nombrePerro=models.CharField(max_length=30)
    descPerro=models.CharField(max_length=200)
    idEstado=models.ForeignKey(Estado,on_delete=models.CASCADE)
    idRaza=models.ForeignKey(Raza,on_delete=models.CASCADE)
    imagenPerro=models.FileField(upload_to='imagenes/')
    def __str__(self):
        return self.nombrePerro

class FichaAnimal(models.Model):
    name=models.IntegerField(primary_key=True,verbose_name="idFicha") #name: solo primaria(siempre)
    fechaFicha=models.CharField(verbose_name="fechaFicha",max_length=30) #revisar en caso de terremoto en el codigo
    idPerro=models.ForeignKey(Perro,on_delete=models.CASCADE)

    def __str__(self):
        return self.fechaFicha

class AdopcionPerro(models.Model):
    name=models.IntegerField(primary_key=True,verbose_name="idAdopcion") #name: solo primaria(siempre)
    FechaAdopcion=models.CharField(verbose_name="FechaAdopcion",max_length=30) #revisar en caso de terremoto en el codigo
    idFichaAnimal=models.ForeignKey(FichaAnimal,on_delete=models.CASCADE)
    Email=models.ForeignKey(Usuario,on_delete=models.CASCADE)

    def __str__(self):
        return self.FechaAdopcion