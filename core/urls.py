from django.contrib import admin
from django.urls import path
from .views import index,adoptar,listarMascota,registroMascota,modificarMascota,eliminarMascota,formulario2,login,logout,error_acceso,adopciones,recuperar

urlpatterns = [
    path('', index,name='home'),
    path('listM/', listarMascota,name='listM'),
    path('regM/', registroMascota,name='regM'),
    path('modM/', modificarMascota,name='modM'),
    path('eliM/', eliminarMascota,name='eliM'),
    path('adop/', adoptar,name='adop'),
    path('formulario2/', formulario2,name='form2'),
    path('logout/',logout,name='logout'),
    path('accounts/login/',error_acceso,name='error'),
    path('login/',login,name='login'),
    path('adopciones/',adopciones,name='adopciones'),
    path('recuperar/',recuperar,name='recuperar'),
]