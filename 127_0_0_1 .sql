-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 26-10-2018 a las 21:21:53
-- Versión del servidor: 5.7.21
-- Versión de PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `misperris`
--
CREATE DATABASE IF NOT EXISTS `misperris` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `misperris`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comuna`
--

DROP TABLE IF EXISTS `comuna`;
CREATE TABLE IF NOT EXISTS `comuna` (
  `idComuna` int(3) NOT NULL,
  `Descripcion` varchar(45) NOT NULL,
  `idRegion` int(2) NOT NULL,
  PRIMARY KEY (`idComuna`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comuna`
--

INSERT INTO `comuna` (`idComuna`, `Descripcion`, `idRegion`) VALUES
(1, 'Arica', 1),
(2, 'Camarones', 1),
(3, 'Putre', 1),
(4, 'General Lagos', 1),
(5, 'Iquique', 2),
(6, 'Alto Hospicio', 2),
(7, 'Pozo Almonte', 2),
(8, 'Caminha', 2),
(9, 'Colchane', 2),
(10, 'Huara', 2),
(11, 'Pica', 2),
(12, 'Antofagasta', 3),
(13, 'Mejillones', 3),
(14, 'Sierra Gorda', 3),
(15, 'Taltal', 3),
(16, 'Calama', 3),
(17, 'Ollagu e', 3),
(18, 'San Pedro de Atacama', 3),
(19, 'Tocopilla', 3),
(20, 'Maria Elena', 3),
(21, 'Copiapo', 4),
(22, 'Caldera', 4),
(23, 'Tierra Amarilla', 4),
(24, 'Chaniaral', 4),
(25, 'Diego de Almagro', 4),
(26, 'Vallenar', 4),
(27, 'Alto del Carmen', 4),
(28, 'Freirina', 4),
(29, 'Huasco', 4),
(30, 'La Serena', 5),
(31, 'Coquimbo', 5),
(32, 'Andacollo', 5),
(33, 'La Higuera', 5),
(34, 'Paiguano', 5),
(35, 'Vicunia', 5),
(36, 'Illapel', 5),
(37, 'Canela', 5),
(38, 'Los Vilos', 5),
(39, 'Salamanca', 5),
(40, 'Ovalle', 5),
(41, 'Combarbala', 5),
(42, 'Monte Patria', 5),
(43, 'Punitaqui', 5),
(44, 'Rio Hurtado', 5),
(45, 'Valparaiso', 6),
(46, 'Casablanca', 6),
(47, 'Concon', 6),
(48, 'Juan Fernandez', 6),
(49, 'Puchuncavi', 6),
(50, 'Quintero', 6),
(51, 'Vinia del Mar', 6),
(52, 'Isla de Pascua', 6),
(53, 'Los Andes', 6),
(54, 'Calle Larga', 6),
(55, 'Rinconada', 6),
(56, 'San Esteban', 6),
(57, 'La Ligua', 6),
(58, 'Cabildo', 6),
(59, 'Papudo', 6),
(60, 'Petorca', 6),
(61, 'Zapallar', 6),
(62, 'Quillota', 6),
(63, 'Calera', 6),
(64, 'Hijuelas', 6),
(65, 'La Cruz', 6),
(66, 'Nogales', 6),
(67, 'San Antonio', 6),
(68, 'Algarrobo', 6),
(69, 'Cartagena', 6),
(70, 'El Quisco', 6),
(71, 'El Tabo', 6),
(72, 'Santo Domingo', 6),
(73, 'San Felipe', 6),
(74, 'Catemu', 6),
(75, 'Llaillay', 6),
(76, 'Panquehue', 6),
(77, 'Putaendo', 6),
(78, 'Santa Maria', 6),
(79, 'Limache', 6),
(80, 'Quilpue', 6),
(81, 'Villa Alemana', 6),
(82, 'Olmue', 6),
(83, 'Santiago', 7),
(84, 'Cerrillos', 7),
(85, 'Cerro Navia', 7),
(86, 'Conchali', 7),
(87, 'El Bosque', 7),
(88, 'Estacion Central', 7),
(89, 'Huechuraba', 7),
(90, 'Independencia', 7),
(91, 'La Cisterna', 7),
(92, 'La Florida', 7),
(93, 'La Granja', 7),
(94, 'La Pintana', 7),
(95, 'La Reina', 7),
(96, 'Las Condes', 7),
(97, 'Lo Barnechea', 7),
(98, 'Lo Espejo', 7),
(99, 'Lo Prado', 7),
(100, 'Macul', 7),
(101, 'Maipu', 7),
(102, 'Niunioa', 7),
(103, 'Pedro Aguirre Cerda', 7),
(104, 'Penialolen', 7),
(105, 'Providencia', 7),
(106, 'Pudahuel', 7),
(107, 'Quilicura', 7),
(108, 'Quinta Normal', 7),
(109, 'Recoleta', 7),
(110, 'Renca', 7),
(111, 'San Joaquin', 7),
(112, 'San Miguel', 7),
(113, 'San Ramon', 7),
(114, 'Vitacura', 7),
(115, 'Puente Alto', 7),
(116, 'Pirque', 7),
(117, 'San Jose de Maipo', 7),
(118, 'Colina', 7),
(119, 'Lampa', 7),
(120, 'Tiltil', 7),
(121, 'San Bernardo', 7),
(122, 'Buin', 7),
(123, 'Calera de Tango', 7),
(124, 'Paine', 7),
(125, 'Melipilla', 7),
(126, 'Alhue', 7),
(127, 'Curacavi', 7),
(128, 'Maria Pinto', 7),
(129, 'San Pedro', 7),
(130, 'Talagante', 7),
(131, 'El Monte', 7),
(132, 'Isla de Maipo', 7),
(133, 'Padre Hurtado', 7),
(134, 'Peniaflor', 7),
(135, 'Rancahua', 8),
(136, 'Codegua', 8),
(137, 'Coinco', 8),
(138, 'Coltauco', 8),
(139, 'Doniihue', 8),
(140, 'Graneros', 8),
(141, 'Graneros', 8),
(142, 'Las Cabras', 8),
(143, 'Machali', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `IdRegion` int(2) NOT NULL,
  `Descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`IdRegion`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `region`
--

INSERT INTO `region` (`IdRegion`, `Descripcion`) VALUES
(7, 'Metropolitana de Santiago'),
(6, 'Valparaiso'),
(5, 'Coquimbo'),
(4, 'Atacama'),
(3, 'Antofagasta'),
(2, 'Tarapaca'),
(1, 'Arica y Parinacota'),
(8, 'Libertador General Bernardo O Higgins'),
(9, 'Maule'),
(10, 'Bio Bio'),
(11, 'Araucania'),
(12, 'Los Rios'),
(13, 'Los Lagos'),
(14, 'Aisen del General Carlos Ibanies del Campo'),
(15, 'Magallanes y Antartica Chilena');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socio`
--

DROP TABLE IF EXISTS `socio`;
CREATE TABLE IF NOT EXISTS `socio` (
  `email` varchar(255) NOT NULL,
  `password` varchar(20) NOT NULL,
  `rut` varchar(10) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `fechaNaci` varchar(10) NOT NULL,
  `telefono` int(8) NOT NULL,
  `idRegion` int(2) NOT NULL,
  `idComuna` int(3) NOT NULL,
  `idTipoVi` int(1) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `socio`
--

INSERT INTO `socio` (`email`, `password`, `rut`, `nombre`, `fechaNaci`, `telefono`, `idRegion`, `idComuna`, `idTipoVi`) VALUES
('ddd@gmail.com', 'pp1234567', '192370809', 'El Muerto', '1992-09-19', 45654567, 1, 4, 2),
('123@gmail.com', '123', '19860495K', 'diego', '26/05/1998', 45654567, 1, 1, 2),
('email', 'pass', 'run', 'nom', '@fecha', 1111, 1, 1, 1),
('algo@wsdf.cl', '123456789', '19860495k', 'diego', '1998-04-26', 93908265, 6, 45, 1),
('dieg.arce@alumnos.duoc.cl', '123456789', '19860495k', 'diego', '1998-05-26', 93908265, 6, 45, 1),
('TRRE@TT.EQ', 'qqq1234567', '192370809', 'ElQueMurio', '1996-12-07', 53130660, 4, 21, 1),
('qaz@gg.gg', 'www12345', '192370809', 'El Que Murio', '1897-12-06', 53130660, 2, 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipovivienda`
--

DROP TABLE IF EXISTS `tipovivienda`;
CREATE TABLE IF NOT EXISTS `tipovivienda` (
  `idTipoVi` int(1) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`idTipoVi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipovivienda`
--

INSERT INTO `tipovivienda` (`idTipoVi`, `descripcion`) VALUES
(1, 'Casa con Patio Grande'),
(2, 'Casa con Patio Pequenio'),
(3, 'Casa sin Patio'),
(4, 'Departamento');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
